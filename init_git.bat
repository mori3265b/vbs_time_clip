@echo off

set x=%~dp0
REM パス末尾の\を除去し終端ディレクトリをファイル名のようにする(C:\hoge\ -> C:\hoge)
set x=%x:~0,-1%

REM ファイル名部分を取得する~nで末端ディレクトリを取得する
for /F "delims=" %%a in ('echo "%x%"') do set name=%%~na

@echo on

git init
git remote add origin https://mori3265b@bitbucket.org/mori3265b/%name%.git

git add .
git commit -m "init"
git push origin master

pause
