Option Explicit

Dim s
s = my_formated_now

clip_string s
show_clip_string s

Function my_formated_now()
  Dim s
  s= "_"
  s= s & Year(Now())
  s= s & "_"
  s= s & Right("0" & Month(Now()) , 2)
  s= s & Right("0" & Day(Now()) , 2)
  s= s & "_"
  s= s & Right("0" & Hour(Now()) , 2)
  s= s & Right("0" & Minute(Now()) , 2)
  s= s & "_"
  s= s & Right("0" & Second(Now()) , 2)
  my_formated_now = s
End Function

Public Sub clip_string(ByVal s)
  Dim cmd  
  cmd = "cmd /c ""echo " & s & "| clip"""
  CreateObject("WScript.Shell").Run cmd, 0
End Sub

Public Sub show_clip_string(ByVal s)
    Dim sh
    Set sh = WScript.CreateObject("WScript.Shell")
    sh.Popup "文字列をクリップボードにコピーしました。" & vbCrLf & "文字列:" & s, 1
    Set sh = Nothing
End Sub
